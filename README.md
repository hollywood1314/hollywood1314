
## About Hollywood

Hollywood is just a game for blockchain.

## Rule analysis

* 10% direct referral reward
* 8% dual zone match
* 1% seepoint award, 30 generations in total, 15%
* 5% s4 global dividend
* 10% token
* 62% static dividend and direct push dynamic income 30%, 50%, 100%
* If there is no participation for 72 hours, the last 100 players will invest more than 10 and get 3 times out.

### Source code analysis
```javascript
library SafeMath {
    
    function mul(uint256 a, uint256 b) 
        internal 
        pure 
        returns (uint256 c) 
    {
        if (a == 0) {
            return 0;
        }
        c = a * b;
        require(c / a == b, "SafeMath mul failed");
        return c;
    }

    function div(uint256 a, uint256 b) 
        internal 
        pure 
        returns (uint256) 
    {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    /**
    * @dev Subtracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b)
        internal
        pure
        returns (uint256) 
    {
        require(b <= a, "SafeMath sub failed");
        return a - b;
    }

    /**
    * @dev Adds two numbers, throws on overflow.
    */
    function add(uint256 a, uint256 b)
        internal
        pure
        returns (uint256 c) 
    {
        c = a + b;
        require(c >= a, "SafeMath add failed");
        return c;
    }
    
    /**
     * @dev gives square root of given x.
     */
    function sqrt(uint256 x)
        internal
        pure
        returns (uint256 y) 
    {
        uint256 z = ((add(x,1)) / 2);
        y = x;
        while (z < y) 
        {
            y = z;
            z = ((add((x / z),z)) / 2);
        }
    }
    //10000×(1-5%)^180≈0.98
    /**
     * @dev gives square. multiplies x by x
     */
    function sq(uint256 x)
        internal
        pure
        returns (uint256)
    {
        return (mul(x,x));
    }
    
    /**
     * @dev x to the power of y 
     */
    function pwr(uint256 x, uint256 y)
        internal 
        pure 
        returns (uint256)
    {
        if (x==0)
            return (0);
        else if (y==0)
            return (1);
        else 
        {
            uint256 z = x;
            for (uint256 i=1; i < y; i++)
                z = mul(z,x);
            return (z);
        }
    }
}

contract HdGame {

    function calTopPoint(uint256 _pID, uint256 _val)
  		private
  	{
  		uint256 _i = 0;
  		uint256 _refPID = _pID;
  		uint256 _tmpBus;
  		uint256 _res;
  		//uint256 _onebus;
  		while((_refPID = players[_refPID].refID) > 0 && _i < floor){
  			if(_refPID == _pID)break;
  			(,,_tmpBus,) = getCom(_refPID);
  			if(_tmpBus > 0 && _tmpBus >= _i + 1){
              	if(_i%2 != 0){
              		_res = _val.mul(1).div(100);
	                if(_res > 0){
	                    players[_refPID].topoint = players[_refPID].topoint.add(_res);	                    
	                }
              	}
            }
            _i++;
  		}
  	}
  	function calOnePush(uint256 _refPID, uint256 _val)
  		private
  	{
  		players[_refPID].onepush = players[_refPID].onepush.add(_val.mul(10).div(100));
  	}
}


```

## Our open source license


